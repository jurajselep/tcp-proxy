#!/bin/bash

# remove namespace if it exists.
sudo ip netns del ns1 &>/dev/null

# create namespace
sudo ip netns add ns1

# create veth link.
sudo ip link add tun0 type veth peer name tun01

# add peer-1 to NS.
sudo ip link set tun1 netns ns1

# setup IP address of tun0.
sudo ip addr add 10.200.1.1/24 dev tun0
sudo ip link set tun0 up

# setup IP address of tun1.
sudo ip netns exec ns1 ip addr add 10.200.1.2/24 dev tun1
sudo ip netns exec ns1 ip link set tun1 up
#sudo  ip netns exec ns1 ip link set lo up


# add support for external traffic
sudo ip netns exec ns1 ip route add default via 10.200.1.1

# share internet access between host and NS.

# enable IP-forwarding.
sudo sysctl -w net.ipv4.ip_forward=1

# flush forward rules, policy DROP by default.
sudo iptables -P FORWARD DROP
sudo iptables -F FORWARD

# flush nat rules.
sudo iptables -t nat -F

# enable masquerading of 10.200.1.0.
sudo iptables -t nat -A POSTROUTING -s 10.200.1.0/255.255.255.0 -o wlp1s0 -j SNAT --to-source 192.168.100.50

# allow forwarding between wlp1s0 and tun0.
sudo iptables -A FORWARD -i wlp1s0 -o tun0 -j ACCEPT
sudo iptables -A FORWARD -o wlp1s0 -i tun0 -j ACCEPT


sudo ip netns exec ns1 ping 8.8.8.8