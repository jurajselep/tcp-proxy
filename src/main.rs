use std::net::Ipv4Addr;
use std::sync::Arc;
use std::thread;

fn main() -> () {
    // create tun0 device
    let nic_tun0 = Arc::new(
        tun_tap::Iface::new("tun0", tun_tap::Mode::Tun)
            .expect("[error] failed to create tun0 device"),
    );
    let nic_tun0_recv = Arc::clone(&nic_tun0);
    let nic_tun0_send = Arc::clone(&nic_tun0);

    // create tun1 device
    let nic_tun1 = Arc::new(
        tun_tap::Iface::new("tun1", tun_tap::Mode::Tun)
            .expect("[error] failed to create tun1 device"),
    );
    let nic_tun1_recv = Arc::clone(&nic_tun1);
    let nic_tun1_send = Arc::clone(&nic_tun1);

    // create buffer
    let mut buf_tun0 = [0u8; 1504];

    // create buffer
    let mut buf_tun1 = [0u8; 1504];

    // create theard for tun0
    let tun0_thread = thread::spawn(move || {
        // keep receiveing ip packets from kernel
        loop {
            // receive ip packets when kernel sends packet
            let tun0_recv_size = nic_tun0_recv.recv(&mut buf_tun0[..]).unwrap();

            // etherenet frame, read 2 bytes of proto
            let tun0_eth_proto = u16::from_be_bytes([buf_tun0[2], buf_tun0[3]]);

            //  process only ipv4 packets (0x800)
            if tun0_eth_proto != 0x800 {
                continue;
            }

            // parse ipv4 packet
            match etherparse::Ipv4HeaderSlice::from_slice(&buf_tun0[4..tun0_recv_size]) {
                Ok(packet) => {
                    // ipv4 source
                    let tun0_ip_source = packet.source_addr();
                    // ipv4 destination
                    let tun0_ip_destination = packet.destination_addr();
                    // ipv4 protocol
                    let tun0_ip_proto = packet.protocol();

                    // process only ipv4 tcp protocol packets
                    // if tun0_ip_proto != 0x06 {
                    //     continue;
                    // }

                    // only write packet from tun0
                    if tun0_ip_destination == Ipv4Addr::from([10, 200, 1, 2]) {
                        // write ip packets received from kernel (tun1) to tun0
                        let _tun1_send_size = nic_tun1_send.send(&buf_tun0[..]);
                    }

                    // parse tpc header
                    match etherparse::TcpHeaderSlice::from_slice(
                        &buf_tun0[4 + packet.slice().len()..],
                    ) {
                        Ok(payload) => {
                            // print packet
                            eprintln!(
                                "[+][tun0] proto: {} | src: {}:{} -> dst: {}:{} ",
                                tun0_ip_proto,
                                tun0_ip_source,
                                payload.source_port(),
                                tun0_ip_destination,
                                payload.destination_port()
                            );
                        }
                        // this is not TPC packet
                        Err(e) => eprintln!("[-][tun0][error] Invalid Ipv4 TPC packet {:?}", e),
                    }
                }
                // this is not ipv4 packet
                Err(e) => eprintln!("[-][tun0][error] Invalid Ipv4 packet {:?}", e),
            }
        }
    });

    // create theard for tun1
    let tun1_thread = thread::spawn(move || {
        // keep receiveing ip packets from kernel
        loop {
            // receive ip packets when kernel sends packet
            let tun1_recv_size = nic_tun1_recv.recv(&mut buf_tun1[..]).unwrap();

            // etherenet frame, read 2 bytes of proto
            let tun1_eth_proto = u16::from_be_bytes([buf_tun1[2], buf_tun1[3]]);

            //  process only ipv4 packets (0x800)
            if tun1_eth_proto != 0x800 {
                continue;
            }

            // parse ipv4 packet
            match etherparse::Ipv4HeaderSlice::from_slice(&buf_tun1[4..tun1_recv_size]) {
                Ok(packet) => {
                    // ipv4 source
                    let tun1_ip_source = packet.source_addr();
                    // ipv4 destination
                    let tun1_ip_destination = packet.destination_addr();
                    // ipv4 protocol
                    let tun1_ip_proto = packet.protocol();

                    // process only ipv4 tcp protocol packets
                    // if tun1_ip_proto != 0x06 {
                    //     continue;
                    // }

                    // only write packet from tun0
                    if tun1_ip_source == Ipv4Addr::from([10, 200, 1, 2]) {
                        // write ip packets received from kernel (tun1) to tun0
                        let _tun0_send_size = nic_tun0_send.send(&buf_tun1[..]);
                    }

                    // parse tpc header
                    match etherparse::TcpHeaderSlice::from_slice(
                        &buf_tun1[4 + packet.slice().len()..],
                    ) {
                        Ok(payload) => {
                            // print packet
                            eprintln!(
                                "[+][tun1] proto: {} | src: {}:{} -> dst: {}: {} ",
                                tun1_ip_proto,
                                tun1_ip_source,
                                payload.source_port(),
                                tun1_ip_destination,
                                payload.destination_port()
                            );
                        }
                        // this is not TPC packet
                        Err(e) => eprintln!("[-][tun1][error] Invalid Ipv4 TPC packet {:?}", e),
                    }
                }
                // this is not ipv4 packet
                Err(e) => eprintln!("[-][tun1][error] Invalid Ipv4 packet {:?}", e),
            }
        }
    });

    tun0_thread.join().unwrap();

    tun1_thread.join().unwrap();
}
