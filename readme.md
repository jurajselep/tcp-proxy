# Tun TCP proxy

### Config proxy

Set `INTERFACE` and `IP` with access to internet in `run.sh`

### Set DNS resolver for ns1 network namespace 

Create file `/etc/netns/ns1/resolv.conf` and set DNS server 

```
nameserver 1.1.1.1
```


### Run proxy 
```
./run.sh
```

### Run tezos node in ns1 network namespace  
```
sudo ip netns exec ns1 ./tezos-node run

```

### Test Ipv4 tcp packet parsing
```
sudo ip netns exec ns1 curl 172.217.23.196
```


### Show all ESTABILISHED connections for pid
```
sudo netstat -taucpnt | grep "ESTABLISHED" | grep 7790
```


### Refactor to support docker

https://platform9.com/blog/container-namespaces-deep-dive-container-networking/