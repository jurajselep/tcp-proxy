#!/bin/bash

# interface with access to internet
INTERFACE=wlp1s0
# interface ip address
IP=192.168.100.50

# kill programm just in case 
pkill tcp

# build code in release mode 
cargo build --release

# add permission to create un device 
sudo setcap cap_net_raw,cap_net_admin=eip ./target/release/tcp

# run program in background
./target/release/tcp &

# get process id 
pid=$!

# remove namespace if it exists.
sudo ip netns del ns1 &>/dev/null

# create namespace
sudo ip netns add ns1

# create veth link.
# sudo ip link add tun0 type veth peer name tun01

# add peer-1 to NS.
sudo ip link set tun1 netns ns1

# setup IP address of tun0.
sudo ip addr add 10.200.1.1/24 dev tun0
sudo ip link set tun0 up

# setup IP address of tun1.
sudo ip netns exec ns1 ip addr add 10.200.1.2/24 dev tun1
sudo ip netns exec ns1 ip link set tun1 up
#sudo  ip netns exec ns1 ip link set lo up


# add support for external traffic
sudo ip netns exec ns1 ip route add default via 10.200.1.1

# share internet access between host and NS.

# enable IP-forwarding.
sudo sysctl -w net.ipv4.ip_forward=1

# flush forward rules, policy DROP by default.
sudo iptables -P FORWARD DROP
sudo iptables -F FORWARD

# flush nat rules.
sudo iptables -t nat -F

# enable masquerading of 10.200.1.0.
sudo iptables -t nat -A POSTROUTING -s 10.200.1.0/255.255.255.0 -o $INTERFACE -j SNAT --to-source $IP

# allow forwarding between wlp1s0 and tun0.
sudo iptables -A FORWARD -i $INTERFACE -o tun0 -j ACCEPT
sudo iptables -A FORWARD -o $INTERFACE -i tun0 -j ACCEPT


# kill program afte ctrl-c
trap "kill $pid" TERM 

# wait for process to finish
wait $pid